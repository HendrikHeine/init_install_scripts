# Ruby

Installing ruby with defined version from the script:
```bash
wget -q https://gitlab.com/DasMoorhuhn/init_install_scripts/-/raw/main/install_ruby.sh  -O- | bash
```

Install ruby with custom ruby version: (replace the 3.2.0 with the version, you want to install)
```bash
wget -q https://gitlab.com/DasMoorhuhn/init_install_scripts/-/raw/main/install_ruby.sh  -O- | bash -s 3.2.0
```

# Systems

Desktop
```bash
wget -q https://gitlab.com/DasMoorhuhn/init_install_scripts/-/raw/main/linux/install_desktop.sh  -O- | bash
```
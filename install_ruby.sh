RUBY_VERSION=3.3.0
ACTIVE_USER=$USER

if [ ! -z "$1" ]; then
    RUBY_VERSION=$1
    echo Using user defined ruby version: $RUBY_VERSION
fi

echo Installing packages

# https://github.com/rbenv/ruby-build/wiki#ubuntudebianmint
sudo apt-get install -y autoconf \
                        patch \
                        build-essential \
                        rustc \
                        libssl-dev \
                        libyaml-dev \
                        libreadline6-dev \
                        zlib1g-dev \
                        libgmp-dev \
                        libncurses5-dev \
                        libffi-dev \
                        libgdbm6 \
                        libgdbm-dev \
                        libdb-dev \
                        uuid-dev \
                        wget

echo Installing ruby version $RUBY_VERSION using rbenv

# https://github.com/rbenv/rbenv-installer#rbenv-installer
wget -q https://github.com/rbenv/rbenv-installer/raw/HEAD/bin/rbenv-installer -O- | bash
echo 'eval "$(~/.rbenv/bin/rbenv init - bash)"' >>  ~/.bashrc
source ~/.bashrc

echo "If the rbenv command isn't avaiable, reopen your terminal and run these commands:"
echo rbenv install $RUBY_VERSION
echo rbenv global $RUBY_VERSION

rbenv install $RUBY_VERSION
rbenv global $RUBY_VERSION

echo ''
echo '#############################################################'
echo '# update system'
echo '#############################################################'
echo ''
sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get autoremove -y

echo ''
echo '#############################################################'
echo '# system tools'
echo '# - inxi'
echo '# - tree'
echo '# - htop'
echo '# - testdisk'
echo '#############################################################'
echo ''
sudo apt-get install -y inxi tree htop testdisk

echo ''
echo '#############################################################'
echo '# network'
echo '# - Net-Tools'
echo '# - ssh'
echo '# - sshfs'
echo '#############################################################'
echo ''
sudo apt-get install -y net-tools ssh sshfs

echo ''
echo '#############################################################'
echo '# Other Tools'
echo '# - neofetch'
echo '# - sl'
echo '#############################################################'
echo ''
sudo apt-get install -y neofetch sl

echo ''
echo '#############################################################'
echo '# bashrc'
echo '# Alias'
echo '# - ll: detailed ls'
echo '# - apt-update: full apt update process'
echo '# Other'
echo '# - neofetch'
echo '#############################################################'
echo ''
echo -e "\n# User defined\n" >> ~/.bashrc
echo -e "alias ll='ls -lahHx'" >> ~/.bashrc
echo -e "alias apt-update='sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get autoremove -y'" >> ~/.bashrc
echo -e "clear && neofetch" >> ~/.bashrc


echo ''
echo '#############################################################'
echo '# Done'
echo '#############################################################'
echo ''
echo '#############################################################'
echo '# Reboot...'
echo '#############################################################'
echo ''
sudo systemctl reboot

py_version="3.12.2"
if [ ! -z "$1" ]; then
    py_version=$1
    echo Using user defined ruby version: py_version
fi

cd ~/

wget https://www.python.org/ftp/python/3.12.2/Python-$py_version.tar.xz
tar -ztvf Python-$py_version.tar.xz
cd Python-$py_version

./configure
make
make test
sudo make install

cd ~/
rm -rf Python-$py_version
rm -rf Python-$py_version.tar.xz
set -e
user=$(whoami)
path=$(pwd)

echo ''
echo '#############################################################'
echo '# update system'
echo '#############################################################'
echo ''
sudo apt update && sudo apt upgrade -y && sudo apt autoremove -y

echo ''
echo '#############################################################'
echo '# ssh environment'
echo '#############################################################'
echo ''
mkdir /home/$user/.ssh
cd /home/$user/.ssh
sudo ssh-keygen -t rsa -b 2048 -C $user@$HOSTNAME
cd $path
eval $(ssh-agent -s)
ssh-add /home/$user/.ssh/$user@$HOSTNAME

echo ''
echo '#############################################################'
echo '# python3'
echo '# - Python3'
echo '# - Pip'
echo '# - venv'
echo '#############################################################'
echo ''
# sudo apt install -y python3 python3-pip python3-venv
wget -q https://gitlab.com/DasMoorhuhn/init_install_scripts/-/raw/main/linux/install_python.sh  -O- | bash
# python3.12 -m pip install --upgrade pip

echo ''
echo '#############################################################'
echo '# docker'
echo '# - Docker'
echo '# - Docker-Compose'
echo '#############################################################'
echo ''
sudo apt-get -y install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc
# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
sudo usermod -aG docker $user

echo ''
echo '#############################################################'
echo '# system tools'
echo '# - inxi'
echo '# - tree'
echo '# - htop'
echo '# - testdisk'
echo '# - OpenJDK-11-JDK'
echo '# - OpenJDK-11-JRE'
echo '#############################################################'
echo ''
sudo apt install -y inxi tree htop testdisk openjdk-11-jdk openjdk-11-jre

echo ''
echo '#############################################################'
echo '# network'
echo '# - Net-Tools'
echo '# - sshfs'
echo '#############################################################'
echo ''
sudo apt install -y net-tools ssh sshfs

echo ''
echo '#############################################################'
echo '# git'
echo '# - Git Framework'
echo '#############################################################'
echo ''
sudo apt install -y git
git config --global user.email "hendrik@ruckelshaeuser.de"
git config --global user.name "DasMoorhuhn"

echo ''
echo '#############################################################'
echo '# Apps'
echo '# - signal desktop'
echo '# - VS-Code'
echo '# - blender'
echo '# - gimp'
echo '# - packettracer'
echo '# - sublime-merge'
echo '# - sublime-text'
echo '# - sqlitebrowser'
echo '# - KRuler'
echo '# - RPI-Imager'
echo '# - obs-studio'
echo '# - wireshark'
echo '# - filezilla'
echo '# - telegram-desktop'
echo '# - Celluloid'
echo '# - Handbrake'
echo '# - Easytag'
echo '# - Sound-Juicer'
echo '# - WinFF'
echo '#############################################################'
echo ''
sleep 2

#Siganl Desktop
wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg
cat signal-desktop-keyring.gpg | sudo tee -a /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' |\
sudo tee -a /etc/apt/sources.list.d/signal-xenial.list
sudo apt update

#VS Code
sudo apt install software-properties-common apt-transport-https wget
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
sudo apt update

sudo apt install -y signal-desktop
sudo apt install -y code
sudo apt install -y blender blender-data
sudo apt install -y gimp
sudo apt install -y packettracer
sudo apt install -y sublime-text sublime-merge
sudo apt install -y sqlitebrowser
sudo apt install -y kruler
sudo apt install -y rpi-imager
sudo apt install -y obs-studio
sudo apt install -y wireshark
sudo apt install -y filezilla
sudo apt install -y telegram-desktop
sudo apt install -y celluloid
sudo apt install -y handbrake
sudo apt install -y easytag
sudo apt install -y sound-juicer
sudo apt install -y winff

echo ''
echo '#############################################################'
echo '# bashrc'
echo '#############################################################'
echo -e "\n# User defined\n" >> ~/.bashrc
echo -e "alias ll='ls -laHhX'" >> ~/.bashrc
echo -e "alias apt-update='sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get autoremove -y'" >> ~/.bashrc
echo -e "alias docker-cleanup='docker container prune && docker image prune && docker volume prune'" >> ~/.bashrc
echo ''

echo '#############################################################'
echo '# Done'
echo '#############################################################'
echo ''
echo '#############################################################'
echo '# Reboot...'
echo '#############################################################'
echo ''
sudo systemctl reboot

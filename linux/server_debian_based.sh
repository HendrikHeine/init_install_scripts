echo ''
echo '#############################################################'
echo '# update system'
echo '#############################################################'
echo ''
sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get autoremove -y

echo ''
echo '#############################################################'
echo '# python3'
echo '# - Python3'
echo '# - Pip'
echo '# - venv'
echo '#############################################################'
echo ''
sudo apt-get install -y python3 python3-pip python3-venv

echo ''
echo '#############################################################'
echo '# docker'
echo '# - Docker'
echo '# - Docker-Compose'
echo '#############################################################'
echo ''
sudo apt-get install -y docker docker-compose
sudo usermod -aG docker $USER

echo ''
echo '#############################################################'
echo '# system tools'
echo '# - inxi'
echo '# - tree'
echo '# - htop'
echo '# - testdisk'
echo '# - zip'
echo '# - unzip'
echo '# - vim'
echo '#############################################################'
echo ''
sudo apt-get install -y inxi tree htop testdisk zip unzip vim

echo ''
echo '#############################################################'
echo '# network'
echo '# - Net-Tools'
echo '# - sshfs'
echo '#############################################################'
echo ''
sudo apt-get install -y net-tools ssh sshfs

echo ''
echo '#############################################################'
echo '# git'
echo '#############################################################'
echo ''
sudo apt-get install -y git
git config --global user.email "hendrik@ruckelshaeuser.de"
git config --global user.name "DasMoorhuhn"

echo ''
echo '#############################################################'
echo '# Other Tools'
echo '# - thefuck'
echo '# - neofetch'
echo '# - sl'
echo '#############################################################'
echo ''
pip3 install thefuck
sudo apt-get install -y neofetch sl

echo ''
echo '#############################################################'
echo '# Own Tools'
echo '# - PyBackup'
echo '#############################################################'
git clone https://gitlab.com/HendrikHeine/pybackup.git

echo ''
echo '#############################################################'
echo '# bashrc'
echo '#############################################################'
echo -e "\n# User defined\n" >> ~/.bashrc
echo -e "alias ll='ls -laHhX'" >> ~/.bashrc
echo -e "alias apt-update='sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get autoremove -y'" >> ~/.bashrc
echo -e 'eval "$(thefuck --alias)"' >> ~/.bashrc
echo -e "clear && neofetch" >> ~/.bashrc
echo ''
echo '#############################################################'
echo '# Reboot...'
echo '#############################################################'
echo ''
sudo systemctl reboot

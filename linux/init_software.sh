echo ''
echo '#############################################################'
echo '# update system'
echo '#############################################################'
echo ''
sudo apt update && sudo apt upgrade -y && sudo apt autoremove -y

echo ''
echo '#############################################################'
echo '# ssh environment'
echo '#############################################################'
echo ''
path=$(pwd)
mkdir /home/$USER/.ssh
cd /home/$USER/.ssh
sudo ssh-keygen -t rsa -b 2048 -C $USER@$HOSTNAME
cd $path
eval $(ssh-agent -s)
ssh-add /home/$USER/.ssh/$USER@$HOSTNAME

echo ''
echo '#############################################################'
echo '# python3'
echo '# - Python3'
echo '# - Pip'
echo '# - venv'
echo '#'
echo '# Libs'
echo '# - Request'
echo '# - Progressbar'
echo '#############################################################'
echo ''
sudo apt install -y python3 python3-pip python3-venv python3-requests
python3 -m pip install --upgrade pip
python3 -m pip install progressbar

echo ''
echo '#############################################################'
echo '# docker'
echo '# - Docker'
echo '# - Docker-Compose'
echo '#############################################################'
echo ''
sudo apt install -y docker docker-compose
sudo usermod -aG docker $USER

echo ''
echo '#############################################################'
echo '# system tools'
echo '# - inxi'
echo '# - tree'
echo '# - htop'
echo '# - testdisk'
echo '#############################################################'
echo ''
sudo apt install -y inxi tree htop testdisk

echo ''
echo '#############################################################'
echo '# network'
echo '# - Net-Tools'
echo '# - sshfs'
echo '#############################################################'
echo ''
sudo apt install -y net-tools ssh sshfs

echo ''
echo '#############################################################'
echo '# git'
echo '#############################################################'
echo ''
sudo apt install -y git
git config --global user.email "hendrik@ruckelshaeuser.de"
git config --global user.name "hendrik.heine"

echo ''
echo '#############################################################'
echo '# Done'
echo '#############################################################'
echo ''
echo '#############################################################'
echo '# Reboot...'
echo '#############################################################'
echo ''
sudo systemctl reboot
